package controlador;
import modelo.cotizacion;
import vista.dlgCotizaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private cotizacion cot;
    private dlgCotizaciones vista;

    public Controlador(cotizacion cot, dlgCotizaciones vista) {
        this.cot = cot;
        this.vista = vista;
            //Hacer que los botones escuche los botones de la vista

        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);        
    }
    
    private void iniciarVista(){
        vista.setTitle(" == Cotizaciones == ");
        vista.setSize(600, 400);
        vista.setVisible(true);
    }
    
    public void Limpiar(){
        vista.txtCot.setText("");
        vista.txtPrecio.setText("");
        vista.txtDescripcion.setText("");
        vista.txtPorcentaje.setText("");
        vista.txtPagoIni.setText("");
        vista.txtTotalFin.setText("");
        vista.txtPagoMen.setText("");
        vista.Plazo.setEnabled(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        
        if(e.getSource() == vista.btnNuevo){
            vista.txtCot.setEnabled(true);
            vista.txtPagoIni.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPorcentaje.setEnabled(true);
            vista.txtTotalFin.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.txtPagoMen.setEnabled(true);
            vista.txtPagoIni.setEnabled(true);
            vista.txtPagoMen.setEnabled(true);
            vista.txtTotalFin.setEnabled(true);
            vista.Plazo.setEnabled(true);
        }
        
        if(e.getSource()==vista.btnLimpiar){
            Limpiar();
        }
        
        if(e.getSource() == vista.btnGuardar){
            cot.setNumCot(vista.txtCot.getText());
            cot.setDescripcion(vista.txtDescripcion.getText());
            
            try{
                cot.setPorcentaje(Integer.parseInt(vista.txtPorcentaje.getText()));
                cot.setPre(Float.parseFloat(vista.txtPrecio.getText()));
                cot.setPlazo(Integer.parseInt(vista.Plazo.getSelectedItem().toString()));
                JOptionPane.showMessageDialog(vista, "Se agrego exitosamente");
                Limpiar();
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: " + ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error " + ex2.getMessage());
            }
        }
        
        if(e.getSource()== this.vista.btnMostrar){
            
            this.vista.txtCot.setText(this.cot.getNumCot());
            this.vista.txtDescripcion.setText(this.cot.getDescripcion());
            this.vista.txtPorcentaje.setText(String.valueOf(this.cot.getPorcentaje()));
            this.vista.txtPrecio.setText(String.valueOf(this.cot.getPre()));
            this.vista.txtPagoIni.setText(String.valueOf(this.cot.calcularPagoIni()));
            this.vista.txtTotalFin.setText(String.valueOf(this.cot.calcularTotalFin()));
            this.vista.txtPagoMen.setText(String.valueOf(this.cot.pagoMen()));    
            vista.Plazo.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Quieres salir?", "Elija una opcion", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        
        if(e.getSource() == this.vista.btnCancelar){
            this.vista.btnGuardar.setEnabled(false);
            this.vista.btnMostrar.setEnabled(false);
            
            Limpiar();
            
            vista.txtCot.setEnabled(false);
            vista.txtPorcentaje.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.Plazo.setEnabled(false);
            vista.txtPagoIni.setEnabled(false);
            vista.txtPagoMen.setEnabled(false);
            vista.txtTotalFin.setEnabled(false);
        }
    }
   
    public static void main(String[] args) {
        cotizacion cot = new cotizacion();
        dlgCotizaciones vista = new dlgCotizaciones(new JFrame(), true);
        
        Controlador cont = new Controlador(cot, vista);
        cont.iniciarVista();
    }
}
