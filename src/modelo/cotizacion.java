package modelo;

import Controlador.*;

public class cotizacion {
    private String numCot;
    private String desc;
    private float pre;
    private float porcentaje;
    private int plazo;
    
    public cotizacion(){
       
    }
    
    public cotizacion(String numCot, String desc, float pre, float porcentaje, int plazo) {
        this.numCot = numCot;
        this.desc = desc;
        this.pre = pre;
        this.porcentaje = porcentaje;
        this.plazo = plazo;
    }
    
    public String getNumCot() {
        return numCot;
    }
    
    public void setNumCot(String numCot) {
        this.numCot = numCot;
    }
    
    public String getDescripcion() {
        return desc;
    }
    
    public void setDescripcion(String desc) {
        this.desc = desc;
    }
    
    public float getPre() {
        return pre;
    }
    
    public void setPre(float pre) {
        this.pre = pre;
    }
    
    public float getPorcentaje() {
        return porcentaje;
    }
    
    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }
    
    public int getPlazo(){
        return plazo;
    }
    
    public void setPlazo(int plazo){
        this.plazo = plazo;
    }
    
    public double calcularPagoIni(){
        double pagoIni;
        pagoIni = pre * (porcentaje/100);
        return pagoIni;
    }
    public double calcularTotalFin(){
        double fin;
        fin = pre - (pre * (porcentaje/100));
        return fin;
    }
    public double pagoMen(){
        double men;
        men = (pre - (pre * (porcentaje/100)))/plazo;
        return men;
    }
}

